﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Attributes
{
    public enum PartnerType { Trust, Partner, ApiPartner, School, Custom };

    public class PartnerAttribute : Attribute
    {
        public PartnerType Type { get; set; }
        public string CustomType { get; set; }
    }
}
