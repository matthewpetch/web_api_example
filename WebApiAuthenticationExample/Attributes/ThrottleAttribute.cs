﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class ThrottleAttribute : ActionFilterAttribute
    {
        public int Delay { get; set; }
        public string Message { get; set; }

        private static MemoryCache _memoryCache = new MemoryCache(new MemoryCacheOptions());

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var key = new { ActionId = context.ActionDescriptor.Id, IPAddress = context.HttpContext.Request.HttpContext.Connection.RemoteIpAddress };
            
            if (!_memoryCache.TryGetValue(key, out bool entry))
            {
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromMilliseconds(Delay));

                _memoryCache.Set(key, true, cacheEntryOptions);
            }
            else
            {
                if (string.IsNullOrEmpty(Message))
                    Message = string.Format("You may only perform this action every {0} seconds.", (Delay/1000.0).ToString("0.##"));

                context.Result = new ContentResult() { StatusCode = 429, Content = Message };
            }
        }
    }
}
