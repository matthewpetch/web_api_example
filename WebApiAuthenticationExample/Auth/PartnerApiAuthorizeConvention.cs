﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiAuthenticationExample.Attributes;

namespace WebApiAuthenticationExample.Auth
{
    public class AddAuthorizeFiltersControllerConvention : IControllerModelConvention
    {
        public void Apply(ControllerModel controller)
        {
            var controllerPartnerAttributes = controller.Attributes.OfType<PartnerAttribute>();

            if (controllerPartnerAttributes.Count() > 0)
            {
                var partnertAttribute = controllerPartnerAttributes.FirstOrDefault();
                if (partnertAttribute == null) throw new NullReferenceException();

                if (partnertAttribute.Type == PartnerType.Partner)
                    controller.Filters.Add(new AuthorizeFilter("PartnerPolicy"));
            }
            else
            {
                controller.Filters.Add(new AuthorizeFilter("ApiPolicy"));
            }
        }
    }
}
