﻿using DummyDatastore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Auth
{
    public static class ClaimsProvider
    {
        public static IEnumerable<Claim> CreateClaims(InventryUser user)
        {
            var claims = new List<Claim>();

            claims.Add(new Claim("name", user.Name));
            claims.Add(new Claim(ClaimTypes.Email, user.Email));
            claims.Add(new Claim("dbs", user.DbsId.ToString()));
            claims.Add(new Claim("uid", user.Id.ToString(), ClaimValueTypes.Integer));

            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            return claims;
        }

        public static IEnumerable<Claim> CreateClaims(InventryUser user, string apiKey)
        {
            var claims = CreateClaims(user).ToList();

            if (string.IsNullOrWhiteSpace(apiKey))
                return claims;

            StringBuilder sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                Byte[] result = hash.ComputeHash(Encoding.UTF8.GetBytes(apiKey));

                foreach (Byte b in result)
                    sb.Append(b.ToString("x2"));
            }

            var hashedKey = sb.ToString();

            if (hashedKey.Equals("2f443c8efa70f7641adf488aea8e7610a9305ac49dc8c87136f9614361e40696")) // secretapikey
            {
                claims.Add(new Claim("api_acc", "api"));
            }
            else if (hashedKey.Equals("3524bad9662fefda13264088d149b1ca9cabfa6c04f6a559ee22f59741050d40")) // publicpartnerkey
            {
                claims.Add(new Claim("api_acc", "partner"));
            }

            return claims;
        }
    }
}
