﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Auth
{
    public interface IBruteForceStopper
    {
        Task CheckRequest(HostString host);

        Task AuthenticationFailed(HostString host);

        void AuthenticationSuccessful(HostString host);

        bool IsBlocked(HostString host);
    }
}
