﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Auth
{
    /// <summary>
    /// Keeps a count of how many times an IP address failed to authenticate and increases the time to respond to their request each time until they get it correct or don't attempt for 10 minutes
    /// Could be a vulnerability to DDOS attacks, possibly update so that if the same IP has failed X amount of times the connection is automatically & instantly closed without checking their credentials (for X amount of minutes, then start accepting them again)
    /// </summary>
    public class BruteForceStopper : IBruteForceStopper
    {
        private IMemoryCache _memoryCache;

        public BruteForceStopper(IMemoryCache memoryCache)
        {
            this._memoryCache = memoryCache;
        }

        public async Task CheckRequest(HostString host)
        {
            if (_memoryCache.TryGetValue(host, out int count))
            {
                var delay = 100 + (int)(Math.Pow(2, count) * 50);
                await Task.Delay(Math.Min(delay, 120000));
            }
        }

        public bool IsBlocked(HostString host)
        {
            if (_memoryCache.TryGetValue(host, out int count) && count > 10)
            {
                return true;
            }

            return false;
        }

        public async Task AuthenticationFailed(HostString host)
        {
            var cacheEntry = await _memoryCache.GetOrCreateAsync(host, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromMinutes(10);
                return Task.FromResult(1);
            });

            cacheEntry++;
            
            _memoryCache.Set(host, cacheEntry, TimeSpan.FromMinutes(10));
        }

        public void AuthenticationSuccessful(HostString host)
        {
            _memoryCache.Remove(host);
        }
    }
}
