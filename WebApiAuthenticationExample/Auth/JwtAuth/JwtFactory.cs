﻿using DummyDatastore;
using DummyDatastore.Providers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Auth.JwtAuth
{
    public class JwtFactory : IJwtFactory
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IUsersProvider _usersProvider;

        public JwtFactory(IOptions<JwtIssuerOptions> options, IUsersProvider usersProvider)
        {
            _jwtOptions = options.Value;
            _usersProvider = usersProvider;
        }

        public string GenerateEncryptedEncodedToken(ClaimsIdentity identity)
        {
            if (identity == null) throw new ArgumentNullException(nameof(identity));

            var jwt = new JwtSecurityTokenHandler().CreateJwtSecurityToken(_jwtOptions.Issuer, _jwtOptions.Audience, identity, _jwtOptions.NotBefore, _jwtOptions.Expiration, _jwtOptions.IssuedAt, _jwtOptions.SigningCredentials, _jwtOptions.EncryptingCredentials);

            var jwtString = new JwtSecurityTokenHandler().WriteToken(jwt);
            return jwtString;
        }

        public string GenerateEncodedToken(ClaimsIdentity identity)
        {
            if (identity == null) throw new ArgumentNullException(nameof(identity));

            var jwt = new JwtSecurityTokenHandler().CreateJwtSecurityToken(_jwtOptions.Issuer, _jwtOptions.Audience, identity, _jwtOptions.NotBefore, _jwtOptions.Expiration, _jwtOptions.IssuedAt, _jwtOptions.SigningCredentials);

            var jwtString = new JwtSecurityTokenHandler().WriteToken(jwt);
            return jwtString;
        }

        public async Task<ClaimsIdentity> GenerateClaimsIdentity(string name, IEnumerable<Claim> claims)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));

            var identity = new ClaimsIdentity(new GenericIdentity(name, "Token"),
                new[] {
                    new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                });

            if (claims != null && claims.Count() > 0)
                identity.AddClaims(claims);

            return identity;
        }

        public Task<ClaimsIdentity> GenerateClaimsIdentity(InventryUser user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            var userClaims = ClaimsProvider.CreateClaims(user);

            return GenerateClaimsIdentity(user.Username, userClaims);
        }

        public Task<ClaimsIdentity> GenerateClaimsIdentity(InventryUser user, string apiKey)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            var userClaims = ClaimsProvider.CreateClaims(user, apiKey);

            return GenerateClaimsIdentity(user.Username, userClaims);
        }

        private static long ToUnixEpochDate(DateTime dt)
        {
            return (long)Math.Round((dt.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
        }

        
    }
}
