﻿using DummyDatastore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Auth.JwtAuth
{
    public interface IJwtFactory
    {
        string GenerateEncodedToken(ClaimsIdentity identity);
        string GenerateEncryptedEncodedToken(ClaimsIdentity identity);
        Task<ClaimsIdentity> GenerateClaimsIdentity(string name, IEnumerable<Claim> claims);
        Task<ClaimsIdentity> GenerateClaimsIdentity(InventryUser user);
        Task<ClaimsIdentity> GenerateClaimsIdentity(InventryUser user, string apiKey);
    }
}
