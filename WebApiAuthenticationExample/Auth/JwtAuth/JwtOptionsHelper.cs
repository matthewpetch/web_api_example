﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Auth.JwtAuth
{
    public class JwtOptionsHelper
    {
        private static IConfigurationSection _jwtAppSettings = null;

        public static JwtBearerOptions BuildBearerOptions(IConfiguration configuration)
        {
            JwtBearerOptions options = new JwtBearerOptions();

            return BuildBearerOptions(options, configuration);
        }

        public static JwtBearerOptions BuildBearerOptions(JwtBearerOptions options, IConfiguration configuration)
        {
            var jwtAppSettingsOptions = _jwtAppSettings ?? (_jwtAppSettings = configuration.GetSection(nameof(JwtIssuerOptions)));

            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Issuer)],
                ValidateAudience = true,
                ValidAudience = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Audience)],
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtIssuerOptions:Key"])),
                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                TokenDecryptionKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Xp2s5v8y/B?E(H+MbQeThVmYq3t6w9z$"))
        };

            options.TokenValidationParameters = tokenValidationParameters;
            options.Audience = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Audience)];
            options.RequireHttpsMetadata = bool.Parse(jwtAppSettingsOptions[nameof(JwtIssuerOptions.RequireHttpsMetadata)]);
            
            return options;
        }

        public static JwtIssuerOptions BuildIssuerOptions(IConfiguration configuration)
        {
            JwtIssuerOptions options = new JwtIssuerOptions();
            return BuildIssuerOptions(options, configuration);
        }

        // to-do: should probably generate new keys on startup that are used the sign and encrypt - that way tokens will work for the entire duration but there's no chance of the key being found and the system being compromised for more than a day 
        public static JwtIssuerOptions BuildIssuerOptions(JwtIssuerOptions options, IConfiguration configuration)
        {
            var jwtAppSettingsOptions = _jwtAppSettings ?? (_jwtAppSettings = configuration.GetSection(nameof(JwtIssuerOptions)));

            options.Issuer = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Issuer)];
            options.Audience = jwtAppSettingsOptions[nameof(JwtIssuerOptions.Audience)];
            options.SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtIssuerOptions:Key"])), SecurityAlgorithms.HmacSha256);

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Xp2s5v8y/B?E(H+MbQeThVmYq3t6w9z$"));
            options.EncryptingCredentials = new EncryptingCredentials(securityKey, SecurityAlgorithms.Aes256KW, SecurityAlgorithms.Aes256CbcHmacSha512);

            return options;
        }
    }
}
