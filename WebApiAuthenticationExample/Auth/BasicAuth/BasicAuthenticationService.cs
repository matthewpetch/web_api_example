﻿using DummyDatastore;
using DummyDatastore.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Auth.BasicAuth
{
    public class BasicAuthenticationService : IBasicAuthenticationService
    {
        private readonly IUsersProvider _usersProvider;

        public BasicAuthenticationService(IUsersProvider usersProvider)
        {
            _usersProvider = usersProvider;
        }

        public async Task<bool> IsValidUserAsync(string username, string password)
        {
            await Task.Delay(10);

            if (username == "test" && password == "test")
            {
                return true;  
            }

            return false;
        }

        public async Task<InventryUser> AuthenticateUserAsync(string username, string password)
        {
            await Task.Delay(10);

            return _usersProvider.AuthenticateUser(username, password);
        }
    }
}
