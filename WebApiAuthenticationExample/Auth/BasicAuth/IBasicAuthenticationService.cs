﻿using DummyDatastore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Auth.BasicAuth
{
    public interface IBasicAuthenticationService
    {
        Task<bool> IsValidUserAsync(string username, string password);
        Task<InventryUser> AuthenticateUserAsync(string username, string password);
    }
}
