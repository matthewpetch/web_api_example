﻿using Microsoft.AspNetCore.Authentication;

namespace WebApiAuthenticationExample.Auth.BasicAuth
{
    public class BasicAuthenticationOptions : AuthenticationSchemeOptions
    {
        public string Realm { get; set; }
    }
}