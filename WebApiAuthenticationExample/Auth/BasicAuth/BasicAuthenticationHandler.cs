﻿using DummyDatastore;
using DummyDatastore.Providers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using WebApiAuthenticationExample.Auth.JwtAuth;

namespace WebApiAuthenticationExample.Auth.BasicAuth
{
    public class BasicAuthenticationHandler : AuthenticationHandler<BasicAuthenticationOptions>
    {
        private const string AuthorizationHeaderName = "Authorization";
        private const string UsernameHeaderName = "Username";
        private const string PasswordHeaderName = "Password";
        private const string BasicSchemeName = "Basic";
        private readonly IBasicAuthenticationService _authenticationService;
        private readonly IBruteForceStopper _bruteForceStopper;
        private readonly IJwtFactory _jwtFactory;
        private readonly IUsersProvider _usersProvider;

        public BasicAuthenticationHandler(
            IOptionsMonitor<BasicAuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IBasicAuthenticationService authenticationService, 
            IJwtFactory jwtFactory,
            IBruteForceStopper bruteForceStopper,
            IUsersProvider usersProvider)
            : base(options, logger, encoder, clock)
        {
            _authenticationService = authenticationService;
            _bruteForceStopper = bruteForceStopper;

            _jwtFactory = jwtFactory;

            _usersProvider = usersProvider;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string username = null;
            string password = null ; 

            if (Request.Headers.ContainsKey(AuthorizationHeaderName))
            {
                if (AuthenticationHeaderValue.TryParse(Request.Headers[AuthorizationHeaderName], out AuthenticationHeaderValue headerValue))
                {
                    if (!BasicSchemeName.Equals(headerValue.Scheme, StringComparison.OrdinalIgnoreCase))
                    {
                        //Not Basic authentication header
                        return AuthenticateResult.NoResult();
                    }

                    byte[] headerValueBytes = Convert.FromBase64String(headerValue.Parameter);
                    string userAndPassword = Encoding.UTF8.GetString(headerValueBytes);
                    string[] parts = userAndPassword.Split(':');
                    if (parts.Length != 2)
                    {
                        return AuthenticateResult.Fail("Invalid Basic authentication header");
                    }
                    username = parts[0];
                    password = parts[1];
                }
            }
            else if (Request.Headers.ContainsKey("Username") && Request.Headers.ContainsKey("Password")) // support for legacy auth headers 😔
            {
                username = Request.Headers["Username"][0];
                password = Request.Headers["Password"][0];
            }
            else
            {
                return AuthenticateResult.NoResult();
            }

            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
                return AuthenticateResult.NoResult();

            await _bruteForceStopper.CheckRequest(Request.Host); // wait if required

            //bool isValidUser = await _authenticationService.IsValidUserAsync(user, password);
            var user = await _authenticationService.AuthenticateUserAsync(username, password);

            if (user == null)
            {
                await _bruteForceStopper.AuthenticationFailed(Request.Host);
                return AuthenticateResult.Fail("Invalid username or password");
            }

            _bruteForceStopper.AuthenticationSuccessful(Request.Host);

            string apiKey = null;
            if (Request.Headers.ContainsKey("x-api-key"))
            {
                apiKey = Request.Headers["x-api-key"][0];
            }

            var claims = ClaimsProvider.CreateClaims(user, apiKey);
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);
            Response.Headers.Add("jwt", await BuildToken(user.Username, claims));
            return AuthenticateResult.Success(ticket);
        }

        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            Response.Headers["WWW-Authenticate"] = $"Basic realm=\"{Options.Realm}\", charset=\"UTF-8\"";
            await base.HandleChallengeAsync(properties);
        }

        private async Task<string> BuildToken(string user, IEnumerable<Claim> claims)
        {
            var identity = await _jwtFactory.GenerateClaimsIdentity(user, claims);
            return _jwtFactory.GenerateEncryptedEncodedToken(identity);
        }
    }
}