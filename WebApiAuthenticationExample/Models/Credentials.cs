﻿using Microsoft.Extensions.Primitives;
using System.ComponentModel.DataAnnotations;

namespace WebApiAuthenticationExample.Models
{
    public class Credentials
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public string ApiKey { get; set; }
    }
}
