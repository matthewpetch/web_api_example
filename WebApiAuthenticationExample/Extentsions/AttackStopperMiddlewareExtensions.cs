﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiAuthenticationExample.Middleware;

namespace WebApiAuthenticationExample.Extentsions
{
    public static class AttackStopperMiddlewareExtensions
    {
        public static IApplicationBuilder UserAttackStopper(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AttackStopperMiddleware>();
        }

        public static void AddAttackStopper(this IServiceCollection services, IConfigurationSection configurationSection)
        {

        }
    }
}
