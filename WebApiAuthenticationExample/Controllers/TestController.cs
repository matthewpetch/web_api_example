﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        public TestController()
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var x = User.Identity.Name;

            await Task.Delay(50);
            return new OkObjectResult(x);

        }

        // Must be PowerUser AND Admin
        [HttpGet("admin")]
        [Authorize(Roles = "PowerUser")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetAdmin()
        {
            await Task.Delay(10);
            var dbsId = User.FindFirstValue("dbs");
            return new OkObjectResult(dbsId);
        }

        // Must be Staff OR Contractor 
        [HttpGet("staff")]
        [Authorize(Roles = "Staff,Contractor")]
        public async Task<IActionResult> GetStaff()
        {
            await Task.Delay(10);
            var dbsId = User.FindFirstValue("dbs");
            return new OkObjectResult(dbsId);
        }

        [HttpGet("jwt")]
        public async Task<IActionResult> JwtTest()
        {
            await Task.Delay(10);
            var dbsId = User.FindFirstValue("dbs");
            return new OkObjectResult(dbsId);
        }

        [HttpGet("file")]
        public async Task<ActionResult<FileContentResult>> GetFile()
        {
            //return Unauthorized();
            
            var path = "";
            var bytes = await System.IO.File.ReadAllBytesAsync("some path");
            return File(bytes, "contentType");
        }

    }
}
