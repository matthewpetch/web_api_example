﻿using DummyDatastore.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiAuthenticationExample.Attributes;
using WebApiAuthenticationExample.Auth.JwtAuth;
using WebApiAuthenticationExample.Models;

namespace WebApiAuthenticationExample.Controllers
{
    [AllowAnonymous] // allow unauthorized access to this controller 
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IJwtFactory _jwtFactory;
        private readonly IUsersProvider _usersProvider;

        public TokenController(IJwtFactory jwtFactory, IUsersProvider usersProvider)
        {
            _jwtFactory = jwtFactory;
            _usersProvider = usersProvider;
        }

        [HttpPost]
        [Throttle(Delay = 5000)]
        public async Task<ActionResult<string>> CreateToken([FromBody]Credentials credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = _usersProvider.AuthenticateUser(credentials.Username, credentials.Password);
            if (user == null)
            {
                return Unauthorized();
            }
            
            var identity = await _jwtFactory.GenerateClaimsIdentity(user, credentials.ApiKey);
            var jwt = _jwtFactory.GenerateEncryptedEncodedToken(identity);

            return Ok(jwt);
        }
    }
}
