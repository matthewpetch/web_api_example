﻿using DummyDatastore;
using DummyDatastore.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Controllers
{
    //[Route("api/[controller]/[action]/{id?}")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUsersProvider _usersProvider;

        public UsersController(IUsersProvider usersProvider)
        {
            _usersProvider = usersProvider;
        }

        [HttpGet("about_me")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<InventryUser>> AboutMe()
        {
            //User.Claims

            await Task.Delay(1);
            
            var id = User.FindFirstValue("id");
            var user = _usersProvider.GetUser(Int32.Parse(id));

            return Ok(user);
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<string>>> Names()
        {
            await Task.Delay(1);

            var names = new string[] { "yap", "me", "you" };

            return Ok(names);
        }

        [HttpGet("yap")]
        [ProducesResponseType(typeof(string), 200)]
        public async Task<ActionResult<List<string>>> Test()
        {
            await Task.Delay(1);

            var names = new string[] { "test", "me", "you" };
            
            return Ok(names);
        }



        //[HttpGet("/{email}")]
        //[AllowAnonymous]
        //public async Task<ActionResult<string>> Test2([FromQuery]string email)
        //{
        //    await Task.Delay(1);
        //    return Ok(email);
        //}
    }
}