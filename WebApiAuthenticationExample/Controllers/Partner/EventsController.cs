﻿using DummyDatastore.Providers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiAuthenticationExample.Attributes;

namespace WebApiAuthenticationExample.Controllers.Partner
{
    [Partner(Type = PartnerType.Partner)]
    [Route("api/partner/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private IEventProvider _eventProvider;

        public EventsController(IEventProvider eventProvider)
        {
            _eventProvider = eventProvider;
        }

    }
}
