﻿using DummyDatastore.Providers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiAuthenticationExample.Attributes;

namespace WebApiAuthenticationExample.Controllers.Partner
{
    [Partner(Type = PartnerType.Partner)]
    [Route("api/partner/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUsersProvider _usersProvider;

        public UsersController(IUsersProvider usersProvider)
        {
            _usersProvider = usersProvider;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<string>>> Names()
        {
            await Task.Delay(1);

            var names = new string[] { "partner", "me", "you" };

            return Ok(names);
        }
    }
}
