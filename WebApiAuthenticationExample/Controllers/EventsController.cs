﻿using DummyDatastore.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiAuthenticationExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private IEventProvider _eventProvider;

        public EventsController(IEventProvider eventProvider)
        {
            _eventProvider = eventProvider;
        }

        [HttpGet]
        [Authorize(Roles = "Staff,Admin")]
        [ProducesResponseType(200, Type = typeof(List<string>))]
        public async Task<IActionResult> GetEvents()
        {
            await Task.Delay(10);
            var events = await _eventProvider.GetEvents();
            return Ok(events);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(string))]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetEvent(int id)
        {
            await Task.Delay(10);

            if (id != 9)
            {
                return NotFound();
            }

            return Ok("in");
        }

        [HttpGet("user/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<List<string>>> GetEventsForUser(int id)
        {
            await Task.Delay(1);

            if (id != 1)
            {
                return NotFound();
            }

            return Ok(new string[] { "in", "out", "in", "in" });
        }

        // note: this doesn't mean this will always 401 unauthorized/403 forbidden! 
        // it just simply doesn't require a role or authorization, but authentication still has to have passed
        // if we wanted to, we could add a policy/role requirement to the entire controller - which would then apply to this call also.
        [HttpGet("forgot_to_add_auth")]
        public async Task<IActionResult> CallWithNoSpecifiedAuth()
        {
            await Task.Delay(1);
            return new OkObjectResult("yap");
        }

    }
}
