﻿using DummyDatastore;
using DummyDatastore.Providers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using WebApiAuthenticationExample.Auth;
using WebApiAuthenticationExample.Auth.BasicAuth;
using WebApiAuthenticationExample.Auth.JwtAuth;

namespace WebApiAuthenticationExample
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // configure global JWT options 
            services.Configure<JwtIssuerOptions>(options =>
            {
                JwtOptionsHelper.BuildIssuerOptions(options, Configuration);
            });

            // adds basic auth and jwt auth options. If JWT fails for whatever reason, the basic auth is used as a backup 
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                //options.DefaultChallengeScheme = BasicAuthenticationDefaults.AuthenticationScheme; 
            })
            .AddBasic<BasicAuthenticationService>(o =>
            {
                o.Realm = "My App";
            })
            .AddJwtBearer(options =>
            {                
                JwtOptionsHelper.BuildBearerOptions(options, Configuration);
            });

            // customises authorization by adding the policy to calls 
            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiPolicy", policy =>
                {
                    //policy.RequireAuthenticatedUser(); // change to check for API key
                    policy.RequireClaim("api_acc", "api");
                    policy.AuthenticationSchemes = new string[] { "Bearer", "Basic"}; // uses both authentication schemes 
                });
                options.AddPolicy("PartnerPolicy", policy =>
                {
                    //policy.RequireAuthenticatedUser(); // change to check for partner API key
                    policy.RequireClaim("api_acc", "partner");
                    policy.AuthenticationSchemes = new string[] { "Bearer", "Basic" }; // uses both authentication schemes
                });

                //options.AddPolicy("administrators", policy => policy.RequireClaim("id", "0", "1")); // sets policy if ID is 0 or 1
                // can also set polices based off roles, or build our own policy handler that can check something from the database 
                // see https://docs.microsoft.com/en-us/aspnet/core/security/authorization/policies?view=aspnetcore-2.1
            });

            // a singleton remains alive for the duration of the application and can be accessed by other services and controllers
            var datastore = new Datastore("connectionString", "databasePassword", "databaseKey"); // this persistent object could run scheduled tasks 
            services.AddSingleton<IDatastore>(datastore);

            var httpClient = new HttpClient();
            // add defaults here
            services.AddSingleton(httpClient);


            // a scope remains alive for the duration of the request
            // these are instantiated each time a call to a controller that uses these via dependency injection is made
            services.AddScoped<IJwtFactory, JwtFactory>();
            services.AddScoped<IEventProvider, EventProvider>();
            services.AddScoped<IUsersProvider, UsersProvider>();
            services.AddScoped<IBruteForceStopper, BruteForceStopper>();

            services.AddMemoryCache();

            services.AddMvc(options =>
            {
                // if you want to add optional XML formatting support (set request header 'Accept' to 'application/xml'
                //options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
                //options.OutputFormatters.RemoveType<TextOutputFormatter>();
                //options.FormatterMappings.SetMediaTypeMappingForFormat("xml", "application/xml");
                //options.RespectBrowserAcceptHeader = true;

                options.Conventions.Add(new AddAuthorizeFiltersControllerConvention()); // policy filter for partner & non-partner controllers
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();

            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "api/{controller=Home}/{action=Index}/{id?}");
            //});

            //app.UseMvc(routes =>
            //{
            //    //routes.MapRoute(
            //    //    name: "areaRoute",
            //    //    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

            //    //routes.MapRoute(
            //    //    name: "default",
            //    //    template: "{controller=Home}/{action=Index}/{id?}");

            //    routes.MapRoute(
            //        name: "area",
            //        template: "api/{area:exists}/{controller=Home}/{action=Index}/{id?}");

            //    routes.MapAreaRoute(
            //        name: "default",
            //        areaName: "Datastore",
            //        template: "api/{controller=Home}/{action=Index}/{id?}");
            //});
        }
    }
}
