﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DummyDatastore
{
    public class InventryUser
    {
        public InventryUser(string username, string password)
        {
            this.Username = username;
            this.Password = password;

            this.DbsId = Guid.NewGuid();
        }

        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public ICollection<string> Roles { get; set; }
        public Guid DbsId { get; set; }
        public string Email { get; set; }
    }
}
