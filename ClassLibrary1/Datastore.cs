﻿using System;

namespace DummyDatastore
{
    public class Datastore : IDatastore
    {
        public string ConnectionString { get; private set; }
        public string Password { get; private set; }
        public string DecryptKey { get; private set; }

        public Datastore(string connectionString, string password, string decryptKey)
        {
            ConnectionString = connectionString;
            Password = password;
            DecryptKey = decryptKey;
        }
    }
}
