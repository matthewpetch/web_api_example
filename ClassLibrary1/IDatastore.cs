﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DummyDatastore
{
    public interface IDatastore
    {
        string ConnectionString
        {
            get;
        }

        string Password
        {
            get;
        }

        string DecryptKey
        {
            get;
        }
    }
}
