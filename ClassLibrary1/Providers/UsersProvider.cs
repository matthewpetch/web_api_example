﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Linq;
using Microsoft.Extensions.Primitives;

namespace DummyDatastore.Providers
{
    public class UsersProvider : IUsersProvider
    {
        private IDatastore _datastore;
        private readonly IDictionary<string, InventryUser> apiUsers;

        public UsersProvider(IDatastore datastore)
        {
            _datastore = datastore;

            apiUsers = new Dictionary<string, InventryUser>();
            PopulateDummyData();
        }

        public InventryUser AuthenticateUser(string username, string password)
        {
            InventryUser user = null;

            if (apiUsers.TryGetValue(username, out user) && user.Password.Equals(password))
            {
                return user;
            }

            return null;
        }

        public InventryUser GetUser(int id)
        {
            var users = apiUsers.Values.Where(x => x.Id == id);

            return users?.First() ?? null;
        }

        private void PopulateDummyData()
        {
            apiUsers.Add("test", new InventryUser("test", "test") { Name = "Matthew Petch", Id = 1, Roles = new List<string>() { "Admin", "PowerUser", "Staff" }, Email = "matthew.petch@inventry.co.uk" });
            apiUsers.Add("test2", new InventryUser("test2", "test") { Name = "Jim", Id = 2, Roles = new List<string>() { "Contractor" }, Email = "someguyh@inventry.co.uk" });
        }
    }
}
