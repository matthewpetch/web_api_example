﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace DummyDatastore.Providers
{
    public interface IUsersProvider
    {
        InventryUser AuthenticateUser(string username, string password);
        InventryUser GetUser(int id);
    }
}
