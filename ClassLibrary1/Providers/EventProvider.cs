﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DummyDatastore.Providers
{
    public class EventProvider : IEventProvider
    {
        private IDatastore _datastore;

        public EventProvider(IDatastore datastore)
        {
            _datastore = datastore;
        }

        public async Task<List<string>> GetEvents()
        {
            // use datastore's connection details to connect to DB and get the events
            await Task.Delay(10);

            return new List<string>() { "In", "In", "Out", "In", _datastore.ConnectionString };
        }
    }
}
