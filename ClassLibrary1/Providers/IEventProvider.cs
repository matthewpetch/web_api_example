﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DummyDatastore.Providers
{
    public interface IEventProvider
    {
        Task<List<string>> GetEvents();
    }
}
